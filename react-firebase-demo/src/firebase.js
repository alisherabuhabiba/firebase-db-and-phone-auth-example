import firebase from 'firebase/app';
import 'firebase/auth';
const prodConfig = {
  apiKey: "AIzaSyC2XT2Q-Va1raTJ5leKsfWZbRFa_5wsr6o",
  authDomain: "mytestfirebase-96850.firebaseapp.com",
  databaseURL: "https://mytestfirebase-96850.firebaseio.com",
  projectId: "mytestfirebase-96850",
  storageBucket: "mytestfirebase-96850.appspot.com",
  messagingSenderId: "39790914483",
  appId: "1:39790914483:web:c7905b5dab6c231b80b9c6",
  measurementId: "G-5K1E825ZDR"
};
const devConfig = {
  apiKey: "AIzaSyC2XT2Q-Va1raTJ5leKsfWZbRFa_5wsr6o",
  authDomain: "mytestfirebase-96850.firebaseapp.com",
  databaseURL: "https://mytestfirebase-96850.firebaseio.com",
  projectId: "mytestfirebase-96850",
  storageBucket: "mytestfirebase-96850.appspot.com",
  messagingSenderId: "39790914483",
  appId: "1:39790914483:web:c7905b5dab6c231b80b9c6",
  measurementId: "G-5K1E825ZDR"
};
const config = process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
if (!firebase.apps.length) {
  firebase.initializeApp(config);
}
const firebaseAuth = firebase.auth();
export default { firebaseAuth, firebase };
