import React from 'react';
import firebase from 'firebase/app'
import {firebaseAuth} from './firebase';
import axios from 'axios'

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: ''
        }
    }
    recaptchaVerifier;

    setUpRecaptcha = () => {

        let cont = document.getElementById('sign-in-button');
        this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(cont, {
            'size': 'invisible',
            'callback': function (response) {
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                this.onSignInSubmit();
            }
        });
    }
    onSignInSubmit = (event) => {
        event.preventDefault()
        this.setUpRecaptcha()
        var phoneNumber = this.state.phone;
        var appVerifier = this.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then(function (confirmationResult) {
                // SMS sent. Prompt user to type the code from the message, then sign the
                // user in with confirmationResult.confirm(code).
                window.confirmationResult = confirmationResult;
                var code = window.prompt("Enter OTP");
                confirmationResult.confirm(code).then(function (result) {
                    // User signed in successfully.
                    var user = result.user;
                    console.log(user,"asdadsadsfasdfasdfasdfasdfasdf")
                    // ...
                    axios.get("http://localhost:8080/checkFirebaseCode/"+this.state.phone).then(res=>{
                        console.log(res,"RES++++++++++++")
                    })
                }).catch(function (error) {
                    // User couldn't sign in (bad verification code?)
                    // ...
                    console.log("Error recieving")
                });
            }).catch(function (error) {
            console.log("Errorroriririri  Sending")
            console.log(error)
        });

    }
    getPHoneToState = (e) => {
        this.setState({phone: e.target.value})
    }

    render() {
        return (
            <div className="container">
                <h1>Hello hello</h1>
                <form onSubmit={this.onSignInSubmit}>
                    <div id="sign-in-button"></div>
                    <div className="form-group">
                        <input type="text" onChange={this.getPHoneToState} name="phone" className="form-control"/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }
}
