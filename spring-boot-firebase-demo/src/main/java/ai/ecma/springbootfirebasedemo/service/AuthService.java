package ai.ecma.springbootfirebasedemo.service;

import ai.ecma.springbootfirebasedemo.payload.ReqPhoneAndCode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.stereotype.Service;


@Service
public class AuthService {

    public UserRecord signUp(String phone) {

            UserRecord userRecord;
            try {
                userRecord = FirebaseAuth.getInstance().getUserByPhoneNumber(phone);
                return userRecord;
            } catch (FirebaseAuthException ignored) {
                return null;
            }

    }
}
