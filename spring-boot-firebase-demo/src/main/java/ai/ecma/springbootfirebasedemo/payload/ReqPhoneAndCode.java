package ai.ecma.springbootfirebasedemo.payload;

import lombok.Data;

@Data
public class ReqPhoneAndCode {
    private String phoneNumber;
    private String code;
}
