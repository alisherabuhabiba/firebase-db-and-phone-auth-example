package ai.ecma.springbootfirebasedemo.controller;

import ai.ecma.springbootfirebasedemo.entity.Employee;
import ai.ecma.springbootfirebasedemo.payload.ReqPhoneAndCode;
import ai.ecma.springbootfirebasedemo.service.AuthService;
import ai.ecma.springbootfirebasedemo.service.FirebaseInitializer;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.auth.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
public class DemoController {

    @Autowired
    FirebaseInitializer firebaseInitializer;

    @Autowired
    AuthService authService;

//    @GetMapping("/getAllEmployees")
//    public List<Employee> getAllEmployees() throws ExecutionException, InterruptedException {
//        List<Employee> employeeList=new ArrayList<>();
//        CollectionReference employeeCollection = firebaseInitializer.getFirebase().collection("Employee");
//        ApiFuture<QuerySnapshot> querySnapshotApiFuture=employeeCollection.get();
//        for (DocumentSnapshot documentSnapshot:querySnapshotApiFuture.get().getDocuments()){
//            Employee employee = documentSnapshot.toObject(Employee.class);
//            employeeList.add(employee);
//        }
//        return employeeList;
//    }
//
//    @PostMapping("saveEmployee")
//    public int saveEmployee(@RequestBody Employee employee){
//        CollectionReference employeeCollection = firebaseInitializer.getFirebase().collection("Employee");
//        employeeCollection.document(String.valueOf(employee.getId())).set(employee);
//        return employee.getId();
//    }

    @GetMapping("/checkFirebaseCode/{phone}")
    public UserInfo checkFirebaseCode(@PathVariable String phone){
        return authService.signUp(phone);
    }
}
