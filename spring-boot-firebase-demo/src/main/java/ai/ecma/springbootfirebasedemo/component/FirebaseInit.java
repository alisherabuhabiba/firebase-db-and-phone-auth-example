package ai.ecma.springbootfirebasedemo.component;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;


@Component
public class FirebaseInit implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(new ClassPathResource("mytestfirebase-96850-firebase-adminsdk-qjr07-5c475cf05b.json").getInputStream()))
                .setDatabaseUrl("https://mytestfirebase-96850.firebaseio.com")
                .build();

        if (FirebaseApp.getApps().isEmpty()){
            FirebaseApp.initializeApp(options);
        }
    }
}
